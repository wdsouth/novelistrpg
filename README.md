# novelistrpg

A RPG system building tool designed for authors. 

Version 0.0.1

This system is designed to be a tool to help authors create RPG games out of
their material. 

At this time there are no plans to build an application out of this.

Each chapter will be available as a separate file for editing and managing. 

Upon full releases there will be a .PDF, .DOC(X), .ODT, and .ePub version of
each finished version. 